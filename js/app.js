var action_vote;

// Define a data structure to manage the content for the voting boxes.
var character_votes_obj = {
  "kaney": {
    "up": 64,
    "down": 36
  }, 
  "mark": { 
    "up": 36, 
    "down": 64
  },
  "cristina": {
    "up": 36,
    "down": 64
  },
  "malala": {
    "up": 64,
    "down": 36
  }
}

function voteUpClick () {
  // enable the vote now button when a user select up vote
  var selector_vote_actions = this.parentElement;
  selector_vote_actions.querySelector(".vote-action__vote").disabled = false;
  action_vote = 'up';
}

function voteDownClick () {
  // enable the vote now button when a user select down vote
  var selector_vote_actions = this.parentElement;
  selector_vote_actions.querySelector(".vote-action__vote").disabled = false;
  action_vote = 'down';
}

function voteNow (event) {
  if (action_vote !== '') {
    // select the character of the vote event
    var selector_character = this.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement;
    toggleVoteGroupAction(selector_character, "vote-group__action", "vote-group__action__hide");

    // fecth the updated data from the local storage
    character_votes_obj = JSON.parse(localStorage.getItem('characters_votes'));

    // update the votes acumulators for the character
    var character = selector_character.id;
    character_votes_obj[character][action_vote] += 1;

    // refresh data votes in the page
    refreshDataVotes(selector_character, character);

    // set the local storage data for update changes
    localStorage.setItem('characters_votes', JSON.stringify(character_votes_obj));
  }
}

function voteAgain () {
  // select the character of the vote event
  var selector_character = this.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement;
  toggleVoteGroupAction(selector_character, "vote-group__action", "vote-group__action__hide");
  
  // disable the vote now button by default
  selector_character.querySelector(".vote-action__vote").disabled = true;
  action_vote = '';
}

function toggleVoteGroupAction (element, actual_class, new_class) {
  // search and toogle the vote-group__action to hide the actual and show thanks fot voting
  var selector_vote_group_action = element.querySelectorAll("." + actual_class);
  for (var index in selector_vote_group_action) {
    if (selector_vote_group_action[index].classList !== undefined) {
      selector_vote_group_action[index].classList.toggle(new_class);
    }
  }
}

function calculatePercentage (character, vote_type) {
  // calculate the total votes of a character
  var total = character_votes_obj[character]['up'] + character_votes_obj[character]['down'];
  // calculate the average
  var average = Math.round((character_votes_obj[character][vote_type] / total) * 100);
  return average.toString() + '%';;
}

function refreshDataVotes (card_element, character) {
  if (character_votes_obj.hasOwnProperty(character)) {
    for (var vote_type in character_votes_obj[character]) {
      selector_span_votes = ".vote-result__" + vote_type + "__span";
      span_votes = card_element.querySelector(selector_span_votes);
      if (span_votes) {
        var percentage = calculatePercentage(character, vote_type);
        span_votes.innerText = percentage;
        updateProgressBar(character, vote_type, percentage);
      }
    }
  }
}

function updateProgressBar(character, vote_type, percentage) {
  var selector_character = document.getElementById(character);
  var selector_progress_bar = selector_character.querySelector(".vote-result__" + vote_type);
  
  if (selector_progress_bar) {
    var width = 30;
    var max_width = parseInt(percentage.substring(0, percentage.length - 1));
    var id = setInterval(frame, 30);
    function frame() {
      if (width >= max_width) {
        selector_progress_bar.style.width = max_width + '%';
        clearInterval(id);
      } else {
        width++;
        selector_progress_bar.style.width = width + '%';
      }
    }
  }
}

function initialize() {
  var span_votes;
  var selector_span_votes;

  // initialize the data structure of votes in local storage
  if (localStorage.getItem('characters_votes') === null) {
    localStorage.setItem('characters_votes', JSON.stringify(character_votes_obj));
  } else { // if exists, fectch the data of votes from local storage
    character_votes_obj = JSON.parse(localStorage.getItem('characters_votes'));
  }

  // loop the character votes object to show data in page
  for (var key in character_votes_obj) {
    selector_character = document.getElementById(key);
    if (selector_character) {
      // Populate data votes
      refreshDataVotes(selector_character, key);

      // Adding the event listeners to buttons
      button_up_vote = selector_character.querySelector(".vote-actions .vote-action__up");
      button_up_vote.addEventListener('click', voteUpClick, false);

      button_down_vote = selector_character.querySelector(".vote-actions .vote-action__down");
      button_down_vote.addEventListener('click', voteDownClick, false);

      button_vote_now = selector_character.querySelector(".vote-actions .vote-action__vote");
      button_vote_now.addEventListener('click', voteNow, false);

      button_vote_again = selector_character.querySelector(".vote-actions .vote-action__again");
      button_vote_again.addEventListener('click', voteAgain, false);
    }
  }
}

window.onload = initialize();